'use strict';

let navBarCollapsed = true;
const elNavBar = document.querySelector('.nav-bar');
const elNavBarToggleIcon = document.querySelector('.nav-bar-toggle-icon');
const toggleNavBar = () => {
    if (elNavBar.classList.contains('collapsed')) {
        elNavBar.classList.remove('collapsed');
    }
    if (navBarCollapsed === true) {
        elNavBar.classList.remove('zoomOutRight', 'delay-1s');
        elNavBar.classList.add('zoomInRight', 'animated');
        elNavBarToggleIcon.classList.add('rotateOut', 'animated');
        setTimeout(() => {
            elNavBarToggleIcon.classList.remove('fa-bars');
            elNavBarToggleIcon.classList.remove('rotateOut');
            elNavBarToggleIcon.classList.add('fa-times');
            elNavBarToggleIcon.classList.add('rotateIn', 'animated', 'move');
        }, 1000);
    } else {
        elNavBar.classList.remove('zoomInRight');
        elNavBar.classList.add('zoomOutRight', 'animated', 'delay-1s');
        elNavBarToggleIcon.classList.add('rotateOut', 'animated');
        setTimeout(() => {
            elNavBarToggleIcon.classList.remove('fa-times');
            elNavBarToggleIcon.classList.remove('rotateOut', 'move');
            elNavBarToggleIcon.classList.add('fa-bars');
            elNavBarToggleIcon.classList.add('rotateIn', 'animated');
        }, 1000);
    }
    navBarCollapsed = !navBarCollapsed;
};

const initPortfolioCarousel = () => {
    const owl = $('.owl-carousel');
    owl.owlCarousel({
        autoplay: true, // autoplayTimeout: 5000
        loop: true,
        nav: true,
        dots: true,
        dotsEach: true,
        responsive: {
            0: {
                items: 1
            },
        },
        navText: ['Prev', 'Next']
    });
    calculateSliderButtons();
};

const debounce = (func) => {
    let timer;
    return function (event) {
        if (timer) clearTimeout(timer);
        timer = setTimeout(func, 100, event);
    };
};

const initListeners = () => {
    window.addEventListener('resize', debounce(() => {
        calculateSliderButtons();
    }));
};

let dots, nav;
const calculateSliderButtons = () => {
    if (!dots || !nav) {
        dots = $('.owl-dots');
        nav = $('.owl-nav');
    }
    let dotsWidth = dots.width();
    dots.css('left', (window.innerWidth - dotsWidth) / 2);
    let navWidth = nav.width();
    nav.css('left', (window.innerWidth - navWidth) / 2);
};

const init = () => {
    initListeners();
    initPortfolioCarousel();
};

$(document).ready(init);
